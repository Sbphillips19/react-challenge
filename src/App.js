import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import fetchJsonp from 'fetch-jsonp';
import Loader from 'react-loader-spinner';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      postalCode: '',
      representatives: [],
      loading: false
    };
  }

  // handle changes to the input
  handleChange = event => {
    this.setState({ postalCode: event.target.value });
  };

  // when click go submits the data from the input
  // sample zip T2Z4M8
  handleSubmit = () => {
    this.setState({ loading: true });
    fetchJsonp(
      `https://represent.opennorth.ca/postcodes/${this.state.postalCode}/`
    )
      .then(response => {
        return response.json();
      })
      .then(json => {
        // filter representatives based on office equal to MP
        this.setState({
          representatives: json.representatives_centroid.filter(
            item => item.elected_office === 'MP'
          ),
          loading: false
        });
      })
      .catch(ex => {
        console.log(ex);
        alert('Please enter a valid postal code');
      });
  };

  render() {
    console.log(this.state.representatives);
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">React Tech Challenge</h1>
        </header>
        <p className="App-intro">
          Enter your postal code to find your MP!
          <br />
          <input
            type="text"
            value={this.state.postalCode}
            onChange={this.handleChange}
          />
          <br />
          <button onClick={() => this.handleSubmit()}>Go</button>
          <br />
        </p>
        <div className="App-people">
          {this.state.loading ? (
            <Loader type="Puff" color="#00BFFF" height="100" width="100" />
          ) : (
            this.state.representatives.map((item, index) => (
              <div key={index} className="App-card">
                <h2>{item.name}</h2>
                <img src={item.photo_url} />
                <h4>District: {item.district_name}</h4>
                <h4>Party: {item.party_name}</h4>
                <a href={item.personal_url}>Personal URL</a>
                <h4>Email: {item.email}</h4>
              </div>
            ))
          )}
        </div>
      </div>
    );
  }
}

export default App;
